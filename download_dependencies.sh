#! /bin/bash

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DEPS_DIR="${ROOT_DIR}/dependencies"
rm -rf ${DEPS_DIR}
mkdir -p ${DEPS_DIR}
cd ${DEPS_DIR}

git clone --depth=1 https://github.com/google/googletest.git
