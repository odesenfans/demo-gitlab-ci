#include <gtest/gtest.h>

#include "hello_lib.h"

TEST(SayHello, CorrectMessage)
{
    const std::string expected_msg("Hello world!");
    const std::string msg = say_hello();

    ASSERT_EQ(expected_msg, msg);
}
