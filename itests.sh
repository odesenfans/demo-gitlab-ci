#!/bin/bash

echo "Starting integration tests"

itests_exec="build/hello_world"

echo "Trying to execute ./${itests_exec}"

OUTPUT=`./${itests_exec}`
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
  echo "Retval is 0, OK"
else
  echo "Retval is not 0, FAIL"
  exit 1
fi

if [ "$OUTPUT" == "Hello world!" ]; then
  echo "Output is correct, OK"
else
  echo "Output is not right, FAIL"
  exit 1
fi
