#include <iostream>

#include "hello_lib.h"

int main(int, char* [])
{
    std::cout << say_hello() << std::endl;
    return 0;
}
